import React, {Component} from 'react';
import './App.css';
import fireApp from './config/firebase';
import Home from './Home';
import Login from './Login';
import { Container } from 'react-bootstrap';

 class App extends Component{
   constructor(props){
     super(props);
     this.state = {
       user: {}
     }
   }

   componentDidMount(){
    this.AuthListener();
   }
   AuthListener(){
     fireApp.auth().onAuthStateChanged(user =>{
       if(user){
         this.setState({user})
       }
       else{
         this.setState({user: null})
       }
     })
   }
   render(){
  return (
    <Container className="d-flex align-item-center justify-content-center"
    style={{ minHeight: "100vh" }}>
    <div className="App w-100" style={{ maxWidth: "400px"}}>
     {this.state.user ? <Home/> : <Login/>}
    </div>
    </Container>
  );
}
}

export default App;
