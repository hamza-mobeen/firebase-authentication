import firebase from "firebase";


const firebaseConfig = {
    apiKey: "AIzaSyBwtx0yZgT22-AJnwVUIAlOvKUIqzAxY8k",
    authDomain: "auth-development-bedae.firebaseapp.com",
    projectId: "auth-development-bedae",
    storageBucket: "auth-development-bedae.appspot.com",
    messagingSenderId: "286931715522",
    appId: "1:286931715522:web:ea2361c6500599591d025b"
  };
  const fireApp = firebase.initializeApp(firebaseConfig);

  export default fireApp;