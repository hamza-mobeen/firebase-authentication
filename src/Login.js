import React,{Component} from 'react'
import {Button, Card, Form} from 'react-bootstrap';
import fireApp from './config/firebase';
import FireApp from './config/firebase';

class Login extends Component {
    constructor(props){
        super(props);
        this.Login= this.Login.bind(this);
        this.handleChange= this.handleChange.bind(this);
        this.Signup = this.Signup.bind(this);
        this.state = {
            email: '',
            password: ''
        }
    }
    Login(event){
        event.preventDefault();
        FireApp.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then((response) => {
        console.log(response);}).catch(err => { console.log(err)});

    }
    handleChange(e){
        this.setState({
       [e.target.name] : e.target.value
        });
    }
    Signup(e){
        e.preventDefault();
        fireApp.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).then((response) => {
            console.log(response);}).catch(err => { console.log(err)});
    }
    render(){
    return (
        <React.Fragment>       
            <Card>
                <h2 className="text-center mb-4">Sign Up</h2>
                <Form>
                    <Form.Group id="email">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" id="email"
                        name="email"
                        placeholder="Enter your Email" 
                        onChange={this.handleChange}
                        value={this.state.email}></Form.Control>
                    </Form.Group>
                    <Form.Group id="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" 
                        name= "password"
                        placeholder="enter your password"
                        onChange={this.handleChange}
                        value={this.state.password} required></Form.Control>
                    </Form.Group>
                   
                    <Button className="col-4 ml-2" onClick={this.Login}>Login</Button>
                    <Button className="col-4 mr-2" onClick={this.Signup}> Signup</Button>
                </Form>

            </Card>
        <div className= "w-100 text-center mt-2">
            Already have a account ? Login In
           
        </div>
        </React.Fragment>

    )
}
}

export default Login;