import React, { Component } from 'react'
import fireApp from './config/firebase';
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }
    logout(){
 fireApp.auth().signOut();
    }
    render() {
        return (
            <div>
          <h1>You are Logged in</h1>
          <button onClick={this.logout}>Logout</button>
            </div>

        )
    }
}

export default Home;